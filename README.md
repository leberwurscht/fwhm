Functions for computing pulse amplitude envelopes with a given intensity FWHM.

Installation:

```
pip3 install git+https://gitlab.com/leberwurscht/fwhm.git
```

Example usage:

```python
import numpy as np

import fwhm

t = np.linspace(-5,5)

envelope = fwhm.sech(t, fwhm=1.5)

print(fwhm.get_fwhm(t, envelope))
```
