import numpy as np
import scipy.interpolate

def gaussian(t, fwhm):
  tau = fwhm/2/np.sqrt(np.log(np.sqrt(2)))
  return np.exp(-(t/tau)**2)

def lorentzian(t, fwhm):
  tau = fwhm/2
  return tau**2/(t**2 + tau**2)

def cossqr(t, fwhm):
  tau = fwhm/2/np.arccos(1/np.sqrt(np.sqrt(2)))
  return np.cos(t/tau)**2 * (abs(t/tau)<=np.pi/2)

def sech(t, fwhm):
  tau = fwhm/2/np.arccosh(np.sqrt(2))
  return 1/np.cosh(t/tau)

def get_fwhm(t, envelope):
  It = abs(envelope)**2
  spline = scipy.interpolate.UnivariateSpline(t, It-np.max(It)/2, s=0)

  try:
    r1, r2 = spline.roots()
    return r2-r1
  except ValueError:
    return np.nan
